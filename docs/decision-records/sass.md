# [Sass](https://sass-lang.com/)

## Decision

* Minimal usage of Sass

## Context

We want to declare our theme "only once" but the logic to allow toggling
dark-mode (via checkbox) or using the system preference via media query
required us to declare each theme "twice".

Sass [`@mixin`][mixin] allows us to create both CSS rules with a "single
declaration".

There are proposals <sup>[\[1\]][proposal-1] [\[2\]][proposal-2]</sup> to bring mixins to native CSS
so in the near future we hope to go back to plain CSS usage but for the
time being this seems like a good trade-off.

## Impacts

By using Sass we actually completely change this repo from plain static
files that can be served directly to "sources" that require
"compilation" (via [build tools](./build-tools.md)).

While this is a somewhat big change it actually enables us to do other
kinds of processing that might be helpful sooner than later. Examples of
this are:
* Minifiers
* Static asset optimizations
* Linters (? this was already available before but the fact that we
  actually need to work in pipelines is a good excuse)

## References

[Reddit question](https://www.reddit.com/r/csshelp/comments/1bk87jg/can_i_avoid_repetition_with_a_media_query_or/)

[mixin]: https://sass-lang.com/documentation/at-rules/mixin/
[proposal-1]: https://css.oddbird.net/sasslike/mixins-functions/
[proposal-2]: https://github.com/w3c/csswg-drafts/issues/9350#issuecomment-1939628591
