# `make` (GNU Make)

[Your Makefiles are wrong](https://tech.davis-hansson.com/p/make/).

#### Don't use tabs

Make leans heavily on the shell, and in the shell spaces matter. Hence,
the default behavior in Make of using tabs forces you to mix tabs and
spaces, and that leads to readability issues.

Instead, ask make to use > as the block character, by adding this at the
top of your makefile:

```make
ifeq ($(origin .RECIPEPREFIX), undefined)
  $(error This Make does not support .RECIPEPREFIX. Please use GNU Make 4.0 or later)
endif
.RECIPEPREFIX = >
```

You really just need the .RECIPEPREFIX = > part, but the version check
is good since there are a lot of old make binaries floating around.

And you will never again pull your hair out because some editor swapped
a tab for four spaces and made Make do insane things.

#### Always use (a recent) bash

Simpler to use a common base platform instead of trying to create
portable makefiles.
