* [ ] Use "picture" so browser can fallback do a different format
* [ ] homepage href is not working in gitlab since it redirects to "non-project" site
      e.g.: project "new-pages-test" - gitlab pages url: https://pedrodavid96.gitlab.io/new-pages-test/
            homepage href points to https://pedrodavid96.gitlab.io, which isn't the same project
* [ ] Build 500/404 page
* [ ] In construction "ribbon" on the corner
* [ ] Analytics
* [ ] Resources minimization
* [ ] Act upon lighthouse analysis
      - It seems server takes a bit longer than expected to transfer `main.html`... in the end this might just be gitlab pages.
        Maybe test a different server (local hosting, Cloud Providers - s3 / gcs ...)
      - Remote resources are taking a while, they should be minimized as well, and maybe bundled in-place with `index.html`
        > Resources are blocking the first paint of your page. Consider delivering critical JS/CSS inline and deferring all non-critical JS/styles. Learn more.
* [ ] Implement linters (check https://www.w3.org/developers/tools/)

* [ ] Better alternative to simply installing developer dependencies in .gitlab-ci.yml
      Examples:
      - .env
      - dev container
* [ ] Improve dev dependencies installation
      Currently both dependencies need workarounds...
* [ ] Better approach to CSS bundling / modularization / optimization
      Examples:
      1. Currently inline CSS has tons of whitespace
      2. I'd like to have a "navbar" module that is then optimized in a
         single inlined file
